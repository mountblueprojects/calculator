function operate(firstNumber, secondNumber, operator) {
  if (operator === "+") {
    return (parseFloat(firstNumber) + parseFloat(secondNumber)).toString();
  } else if (operator === "-") {
    return (parseFloat(firstNumber) - parseFloat(secondNumber)).toString();
  } else if (operator === "x") {
    return (parseFloat(firstNumber) * parseFloat(secondNumber)).toString();
  } else {
    if (secondNumber === "0") {
      return "Division by zero";
    }
    return (parseFloat(firstNumber) / parseFloat(secondNumber)).toString();
  }
}

function calculate(state, buttonId) {
  if (buttonId === "AC") {
    return {
      current: null,
      accumulator: null,
      operation: null,
    };
  }

  if (buttonId === "+/-") {
    if (state.current) {
      return {
        current: (parseFloat(state.current) * -1).toString(),
      };
    } else if (state.accumulator) {
      return {
        accumulator: (parseFloat(state.accumulator) * -1).toString(),
      };
    }
    return {};
  }

  if (buttonId === "%") {
    if (state.accumulator) {
      return {};
    } else if (state.current) {
      if (state.current === "0") {
        return {};
      } else {
        return { current: (parseFloat(state.current) / 100).toString() };
      }
    } else {
      return {};
    }
  }

  if (buttonId === ".") {
    if (state.accumulator) {
      return {};
    } else if (state.current) {
      if (state.current.includes(".")) {
        return {};
      } else {
        return { current: state.current + "." };
      }
    } else {
      return { current: "0." };
    }
  }

  if (!isNaN(buttonId)) {
    if (buttonId === "0" && state.current === "0") {
      return {};
    } else if (state.current) {
      if (state.current === "0") {
        return { current: buttonId };
      } else {
        return { current: state.current + buttonId };
      }
    } else {
      return { current: buttonId };
    }
  }

  if (buttonId === "=") {
    if (state.current && state.operation) {
      return {
        accumulator: operate(state.accumulator, state.current, state.operation),
        current: null,
        operation: null,
      };
    } else {
      return {};
    }
  }

  if (state.operation) {
    return {
      accumulator: operate(state.current, state.accumulator, state.operation),
      current: null,
      operation: buttonId,
    };
  }

  if (buttonId === "+") {
    return { accumulator: state.current, current: null, operation: buttonId };
  }

  if (buttonId === "-") {
    return { accumulator: state.current, current: null, operation: buttonId };
  }

  if (buttonId === "x") {
    return { accumulator: state.current, current: null, operation: buttonId };
  }

  if (buttonId === "÷") {
    return { accumulator: state.current, current: null, operation: buttonId };
  }
}

export default calculate;
