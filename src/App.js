import React from "react";
import ResultSection from "./components/ResultSection";
import KeyPad from "./components/KeyPad";
import "./App.css";
import calculate from "./logic/calculate";
import PropTypes from "prop-types";
// function operate(current, next, operation) {}

//state={total:null,next:null,operation:null}

class App extends React.Component {
  constructor() {
    super();
    this.state = { accumulator: null, current: null, operation: null };
  }

  handleClick = (buttonId) => {
    this.setState(calculate(this.state, buttonId));
  };

  render() {
    return (
      <div id="calculator">
        <ResultSection
          value={this.state.current || this.state.accumulator || "0"}
        />
        <KeyPad functionForButtons={this.handleClick} />
      </div>
    );
  }
}

//PropTypes
KeyPad.propTypes = {
  functionForButtons: PropTypes.func,
};

ResultSection.propTypes = {
  value: PropTypes.string,
};

export default App;
