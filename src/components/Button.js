import React from "react";
import "./Button.css";

class Button extends React.Component {
  handleClick = () => {
    this.props.buttonFunction(this.props.id);
  };

  render() {
    const componentClass = [
      "button",
      this.props.operator ? "operator" : "",
      this.props.zero ? "zero" : "",
    ];

    return (
      <div className={componentClass.join(" ")}>
        <button onClick={this.handleClick}> {this.props.id} </button>
      </div>
    );
  }
}

export default Button;
