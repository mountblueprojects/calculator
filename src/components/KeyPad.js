import React from "react";
import Button from "./Button";
import PropTypes from "prop-types";
import "./KeyPad.css";

class KeyPad extends React.Component {
  handleClick = (buttonId) => {
    this.props.functionForButtons(buttonId);
  };

  render() {
    return (
      <div id="keypad">
        <div>
          <Button id="AC" buttonFunction={this.handleClick} />
          <Button id="+/-" buttonFunction={this.handleClick} />
          <Button id="%" buttonFunction={this.handleClick} />
          <Button id="÷" buttonFunction={this.handleClick} operator />
        </div>

        <div>
          <Button id="7" buttonFunction={this.handleClick} />
          <Button id="8" buttonFunction={this.handleClick} />
          <Button id="9" buttonFunction={this.handleClick} />
          <Button id="x" buttonFunction={this.handleClick} operator />
        </div>

        <div>
          <Button id="4" buttonFunction={this.handleClick} />
          <Button id="5" buttonFunction={this.handleClick} />
          <Button id="6" buttonFunction={this.handleClick} />
          <Button id="-" buttonFunction={this.handleClick} operator />
        </div>

        <div>
          <Button id="1" buttonFunction={this.handleClick} />
          <Button id="2" buttonFunction={this.handleClick} />
          <Button id="3" buttonFunction={this.handleClick} />
          <Button id="+" buttonFunction={this.handleClick} operator />
        </div>

        <div>
          <Button id="0" buttonFunction={this.handleClick} zero />
          <Button id="." buttonFunction={this.handleClick} />
          <Button id="=" buttonFunction={this.handleClick} operator />
        </div>
      </div>
    );
  }
}

// PropTypes
Button.propTypes = {
  id: PropTypes.string,
  operator: PropTypes.bool,
  zero: PropTypes.bool,
  buttonFunction: PropTypes.func,
};

export default KeyPad;
