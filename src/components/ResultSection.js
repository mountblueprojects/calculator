import React from "react";
import "./ResultSection.css";

class ResultSection extends React.Component {
  render() {
    return (
      <div id="result-screen">
        <div>{this.props.value}</div>
      </div>
    );
  }
}

export default ResultSection;
